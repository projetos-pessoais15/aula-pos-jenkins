package com.aula.pos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class PosApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(PosApplication.class, args);
	}
	
	@RequestMapping("/ola-mundo")
	public ResponseEntity<String> hello() {
		return ResponseEntity.ok().body("Jenkins - CI/CD 27/08/2022");
	}

}
